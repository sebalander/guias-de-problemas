
\title{\huge GUÍA DE PROBLEMAS N° 4.1 \\ NIVEL 3 - 2024 \\ \hrulefill \\
Trabajo Teórico Experimental: Experimento de Flotación y Regresión Lineal.}
\author{\large La Física al Alcance de Todos - Prof. Daniel Córdoba}
\date{3 de Agosoto de 2024}

\begin{document}

\maketitle

\thispagestyle{fancy}
\fancyhead[L]{
    \begin{picture}(0,0) \put(0,-0.2\paperwidth)
    {\includegraphics[width=0.2\paperwidth, height=0.2\paperwidth]{gofito_sinfondo.png}}
    \end{picture}
}
    \renewcommand{\headrulewidth}{0.pt}
    \pagestyle{plain}

\section{Toma de Datos}

\subsection{Preparación}
Materiales: 
\begin{enumerate}
    \item Un vaso graduado, 
    \item marcador indeleble,
    \item pote de 1Kg que servirá de cubeta, 
    \item taza con agua extra, y
    \item 10 piedras.
\end{enumerate}

Preparar unas diez piedras y numerarlas con el marcador desde el 1 al 10. Colocar individualmente cada piedra en una balanza y registrar el peso en gramos.

El pote de 1Kg será la cuba de flotación para el vaso graduado, llenarla aproximadamente hasta la mitad con agua. Tener a mano además media taza con agua extra en caso de que haga falta llenar o vaciar más la cubeta.

Colocar dentro del vaso graduado un fondo de \emph{aproximadamente 3cm de arena}. Hacer flotar el vaso graduado dentro de la cuba, verificar que se mantiene aproximadamente vertical, es decir que no tiende a volcarse demasiado de costado chocando con los bordes de la cuba. Si es demasiado inestable, agregar un poco más de arena.

Estimar el área de sección del vaso en milímetros cuadrados, considerar el exterior del vaso, no el interior.

\subsection{Procedimiento de Medición}

Se irán agregando una a una las piedras numeradas adentro del vaso graduado. Con cada nueva piedra el vaso se hundirá un poco más. Se registrará la profundidad sumergida para cada piedra que se agregue. La colocación de las piedras es acumulativa, una vez colocada una piedra, no retirarlas, se deben agregar las demás piedras encima.

Preparar una tabla con las siguientes columnas: peso agregado [gramos], profundidad sumergida [milímetros].
\begin{table}[h!]
\centering
\small
\begin{tabular}{|p{0.25\linewidth}|p{0.25\linewidth}|p{0.25\linewidth}|}
\hline
{Última piedra agregada} & {Peso agregado [g]} & {Profundidad sumergida [mm]} \\ \hline
Ninguna & 0 & $\langle$Profundidad Inicial$\rangle$ \\ \hline
1 & $\langle$Peso primera piedra$\rangle$ & ... \\ \hline
... & ... & ... \\ \hline
\end{tabular}
\end{table}

El procedimiento para la toma de datos es:
\begin{enumerate}
    \item Empezar sin agregar ninguna de las piedras numeradas ({peso agregado = 0 gramos}) registrando la profundidad sumergida en milímetros. Este es el primer dato de la tabla, la primera fila. 
    \item Agregar la piedra numerada 1, el peso agregado en este caso es igual al peso de la piedra 1 en gramos; registrar la nueva profundidad sumergida.
    \item Agregar la segunda piedra numerada, en este caso el peso agregado es la suma de los pesos de las piedras 1 y 2; registrar la profundidad sumergida.
    \item Continuar para todas las piedras numeradas.
\end{enumerate}

\section{Regresión Lineal}

\subsection{Visualización de los datos}

La variable de control \(X\) es el peso agregado y la variable dependiente \(Y\) es la profundidad sumergida.

Construir una gráfica de los datos registrados en una hoja milimetrada. La variable de control va en el eje horizontal y la variable dependiente en el eje vertical. Elegir la escala de los ejes para que la gráfica sea grande y clara. Como ejemplo pueden ver la Figura \ref{fig:scatter}.

\begin{figure}[h!]
    \centering
    \begin{tikzpicture}
        \begin{axis}[
            width=0.9\linewidth, height=0.9\linewidth,
            xlabel={\small Peso Agregado [gramos]},
            ylabel={\small Profundidad Sumergida [mm]},
            grid=major,
            xmin=0, ymin=0, % Ensure the axes start at zero
            legend style={at={(1,1)}, anchor=north east}
        ]
        \addplot[
            only marks,
            mark=*,
            mark size=3pt,
            color=black
        ]
        table {
            x y
            0    45
            15   50
            25   58
            43   70
            55   73
            70   80
            83   84
            95   88
            105  94
            125  100
            138  106
        };
        % Add the dotted line segment
        \addplot[
            color=black,
            dashed,
            line width=1.5pt
        ] coordinates {
            (0, 47)
            (140, 108.5)
        };
        \end{axis}
    \end{tikzpicture}
    \caption{Ejemplo de cómo podrían verse los datos graficados y la recta que mejor los ajusta.}
    \label{fig:scatter}
\end{figure}


\subsection{Cálculo de la recta que mejor ajusta los datos}

Calcular la recta que mejor ajusta los datos con el método de regresión lineal. 
Siendo modelo de la recta
\begin{equation}
Y = a + b X , \label{eq:recta}
\end{equation}
donde \(a\) es la ordenada al origen (en milímetros) y \(b\) la pendiente (en milímetros/gramo). Usar el método de la regresión lineal detallado en el apéndice para calcular la pendiente y la ordenada al origen.

Visualizar la recta obtenida sobre los datos. Marcar en la gráfica el punto de la ordenada al origen \(a\) sobre el eje de la variable dependiente. 
Trazar con regla desde allí una recta con la pendiente calculada \(b\).  
Debería verse como la linea punteada en la Figura \ref{fig:scatter}. 
¿La recta es representativa de los datos? 
¿Cómo explican la discrepancia entre ellos? 

Discutir cuáles son las fuentes de error e incertidumbre en el experimento.

\section{Modelo Teórico y Procesamiento de los Resultados}

Partiendo de los resultados de la regresión deberán deducir los valores de: la densidad del agua y la masa inicial del vaso graduado.

Aplicar el principio de Arquímedes al experimento realizado y demostrar que
\begin{equation}
h = A ~ \rho ~ m_0 + A ~ \rho ~ m_A \label{eq:dependencia}
\end{equation}

donde:
\begin{itemize}
    \item \(h\) es la profundidad sumergida,
    \item \(A\) es el área de sección del vaso graduado,
    \item \(\rho\) es la densidad del agua,
    \item \(m_0\) es la masa inicial, la masa del vaso y el arena colocados antes del proceso de medición, y
    \item \(m_A\) es la masa de las piedras numeradas agregadas en el proceso de medición.
\end{itemize}

Identificar cuáles de estas variables y parámetros se han medido experimentalmente según las instrucciones de las secciones anteriores.

Relacionar las partes de la Ecuación \ref{eq:dependencia} con el modelo de la recta en la Ecuación \ref{eq:recta}. Igualar la pendiente y la ordenada al origen con las variables y despejar las dos incógnitas: \(\rho\) y \(m_0\).

Reportar \(\rho\) y \(m_0\). Interpretar si les parecen razonables.


\begin{appendices}

\input{regresion_lineal}

\end{appendices}

\end{document}
